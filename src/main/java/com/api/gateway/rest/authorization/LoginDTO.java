package com.api.gateway.rest.authorization;

public record LoginDTO(String userName, String password) {}