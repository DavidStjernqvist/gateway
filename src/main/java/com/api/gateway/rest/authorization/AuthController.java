package com.api.gateway.rest.authorization;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {

  final LoginService loginService = new LoginService();

  @CrossOrigin(origins = "*", methods = RequestMethod.POST)
  @PostMapping("/login")
  public ResponseEntity<String> login(@RequestBody final LoginDTO loginDTO) {
    return new ResponseEntity<>(
        this.loginService.login(loginDTO.userName(), loginDTO.password()),
        HttpStatus.OK
    );
  }
}
