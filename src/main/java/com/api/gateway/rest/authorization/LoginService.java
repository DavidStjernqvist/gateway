package com.api.gateway.rest.authorization;

import com.api.gateway.security.JwtTokenUtil;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.function.BiPredicate;

@Component
class LoginService {

  private final JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();

  final BiPredicate<String, String> validateUser = (final String userName, final String password) ->
      userName.equals("root") && password.equals("root");

  protected String login(final String username, final String password) {
    if (validateUser.test(username, password)) {
      return this.jwtTokenUtil.generateToken(username);
    }
    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid credentials");
  }
}
