package com.api.gateway.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenUtil {

  private final String secret = "secret";

  public String generateToken(final String userName) {
    final Claims claims = Jwts.claims()
        .setId("1")
        .setSubject(userName)
        .setAudience("User");

    return generateToken(claims);
  }

  public String generateToken(final Claims claims) {
    final long nowMillis = System.currentTimeMillis();
    long tokenValidity = 10;
    final long expMillis = nowMillis + tokenValidity * 10000;
    final Date issuedDate = new Date(nowMillis);
    final Date expirationDate = new Date(expMillis);

    return Jwts.builder()
        .setClaims(claims)
        .setIssuedAt(issuedDate)
        .setExpiration(expirationDate)
        .signWith(SignatureAlgorithm.HS512, this.secret)
        .compact();
  }

  public void validateToken(final String token) {
    Jwts.parser()
        .setSigningKey(this.secret)
        .parseClaimsJws(token);
  }
}
