package com.api.gateway.security;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class JwtAuthFilter implements GatewayFilter {

  private final JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();

  private final RouterValidator routerValidator = new RouterValidator();

  @Override
  public Mono<Void> filter(final ServerWebExchange exchange, final GatewayFilterChain chain) {
    final ServerHttpRequest request = exchange.getRequest();

    if (this.routerValidator.isSecured.test(request)) {
      final ServerHttpResponse response = exchange.getResponse();
      response.setStatusCode(HttpStatus.UNAUTHORIZED);

      return response.setComplete();
    }

    final String token = request.getHeaders().getOrEmpty("Authorization")
        .stream().findFirst()
        .filter((final String t) -> t.contains("Bearer"))
        .map((final String t) -> t.split(" ")[1])
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED));

    try {
      jwtTokenUtil.validateToken(token);
    } catch (final Exception exception) {
      final ServerHttpResponse response = exchange.getResponse();
      response.setStatusCode(HttpStatus.UNAUTHORIZED);
      return response.setComplete();
    }

    return chain.filter(exchange);
  }
}
