package com.api.gateway.security;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.function.Predicate;

@Component
public class RouterValidator {

  public static final Set<String> openApiEndpoints = Set.of("/auth/login");

  public Predicate<ServerHttpRequest> isSecured =
      (final ServerHttpRequest httpRequest) -> openApiEndpoints.contains(httpRequest.getURI().getPath());

}
