package com.api.gateway.config;

import com.api.gateway.security.JwtAuthFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

  private final JwtAuthFilter jwtAuthFilter = new JwtAuthFilter();

  @Bean
  public RouteLocator routeLocator(final RouteLocatorBuilder builder) {
    return builder.routes()
        .route("auth", r -> r.path("/auth/**").filters(f -> f.filter(this.jwtAuthFilter)).uri("lb://auth"))
        .route(r -> r.path("/**").filters(f -> f.filter(this.jwtAuthFilter)).uri("http://localhost:8081/"))
        .build();
  }
}
